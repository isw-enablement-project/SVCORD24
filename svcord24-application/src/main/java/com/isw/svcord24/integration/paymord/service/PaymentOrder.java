package com.isw.svcord24.integration.paymord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord24.sdk.integration.paymord.service.PaymentOrderBase;
import com.isw.svcord24.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class PaymentOrder extends PaymentOrderBase {

  private static final Logger log = LoggerFactory.getLogger(PaymentOrder.class);

  public PaymentOrder(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord24.sdk.integration.paymord.entity.PaymentOrderOutput execute(com.isw.svcord24.sdk.integration.paymord.entity.PaymentOrderInput paymentOrderInput)  {

    log.info("PaymentOrder.execute()");
    // TODO: Add your service implementation logic
   
    return null;
  }

}
