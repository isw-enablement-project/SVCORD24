package com.isw.svcord24.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord24.sdk.domain.svcord.command.Svcord24CommandBase;
import com.isw.svcord24.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord24.sdk.domain.facade.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord24Command extends Svcord24CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord24Command.class);

  public Svcord24Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord24.sdk.domain.svcord.entity.Svcord24 createServicingOrderProducer(com.isw.svcord24.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord24Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic
      return null;
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord24.sdk.domain.svcord.entity.Svcord24 instance, com.isw.svcord24.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord24Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic
      
    }
  
}
