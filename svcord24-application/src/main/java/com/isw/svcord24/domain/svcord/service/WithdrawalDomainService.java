package com.isw.svcord24.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord24.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord24.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord24.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord24.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord24.domain.svcord.command.Svcord24Command;
import com.isw.svcord24.integration.partylife.service.RetrieveLogin;
import com.isw.svcord24.integration.paymord.service.PaymentOrder;
import com.isw.svcord24.sdk.api.v1.model.CustomerReference;
import com.isw.svcord24.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord24.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }

  @Autowired
  Svcord24Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  
  @NewSpan
  @Override
  public com.isw.svcord24.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord24.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic
    RetrieveLoginInput retriveInput = integrationEntityBuilder.getPartylife().getRetrieveLoginInput().setId("test1").build();

    RetrieveLoginOutput retriveServiceOutput = retrieveLogin.execute(retriveInput);

    if(retriveServiceOutput.getResult() != "SUCCESS"){
      return null;
    }

    CustomerReference customerReference = this.entityBuilder.getSvcord().getCustomerReference().build();
    customerReference.

    this.entityBuilder.getSvcord().getCreateServicingOrderProducerInput()
                      .setCustomerReference();
                      

    return null;
  }

}
